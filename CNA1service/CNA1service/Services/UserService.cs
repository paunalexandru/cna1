﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CNA1service;
using CNA1service.Protos;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace CNA1service.Services
{
    public class UserService : User.UserBase
    {
        private readonly ILogger<UserService> _logger;
        public UserService(ILogger<UserService> logger)
        {
            _logger = logger;
        }

        public override Task<UserReply> TakeInfo(UserRequest request, ServerCallContext context)
        {
            int age;
            string gender;
            
            if (request.Cnp.Length == 13)
            {
                #region Gender
                if (request.Cnp.StartsWith("1") || request.Cnp.StartsWith("5"))
                {
                    gender = "male";
                }
                else
                {
                    if (request.Cnp.StartsWith("2") || request.Cnp.StartsWith("6"))
                    {
                        gender = "female";
                    }
                    else
                    {
                        gender = "unknown";
                    }
                }
                #endregion

                #region Age
                DateTime todayDate = DateTime.Today;
                DateTime zeroDate = new DateTime(1, 1, 1);
                string cnpYearString;
                string cnpMonthString = request.Cnp[3].ToString() + request.Cnp[4].ToString();
                string cnpDayString = request.Cnp[5].ToString() + request.Cnp[6].ToString();
                
                if (request.Cnp.StartsWith("2") || request.Cnp.StartsWith("1"))
                { 
                    cnpYearString = "19" + request.Cnp[1].ToString() + request.Cnp[2].ToString();                 
                }
                else
                { 
                    cnpYearString = "20" + request.Cnp[1].ToString() + request.Cnp[2].ToString(); 
                }

                DateTime cnpDate = new DateTime(Convert.ToInt32(cnpYearString), Convert.ToInt32(cnpMonthString), Convert.ToInt32(cnpDayString));
                TimeSpan substractDates = todayDate - cnpDate;
                age = (zeroDate + substractDates).Year - 1;
                #endregion

                _logger.Log(LogLevel.Information, "\n" + request.Name + " " + request.Cnp + " " + gender + " " + age + "\n");
                return Task.FromResult(new UserReply { Reply = "Datele au fost trimise corect!" });
            }
            else
            {
                _logger.Log(LogLevel.Information, "\n" + request.Name + " " + request.Cnp + "\n");
                return Task.FromResult(new UserReply { Reply = "Datele trimise nu sunt valide!" });
            }
        }
    }
}