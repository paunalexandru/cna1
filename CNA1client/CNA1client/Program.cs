﻿using CNA1service.Protos;
using Grpc.Net.Client;
using System;
using System.Threading.Tasks;

namespace CNA1client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string name;
            string cnp;
            Console.WriteLine("Numele: ");
            name = Console.ReadLine();
            Console.WriteLine("CNP-ul: ");
            cnp = Console.ReadLine();

            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new User.UserClient(channel);
            var response = await client.TakeInfoAsync(new UserRequest { Cnp = cnp, Name = name });
            Console.WriteLine(response);

            Console.ReadLine();
        }
    }
}